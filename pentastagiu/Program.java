package pentastagiu;

// Patachi Alexandra
public class Program {

	public static void main(String[] args) {   
		Input input = new Input();
        String content = input.ReadInputFromConsole();
        
        String currentDir = System.getProperty("user.dir");
        String filePath = currentDir + "\\WriteHere.txt";

        ActionsOnFile action = new ActionsOnFile(filePath);
        action.WriteInputToFile(content);
	}

}
