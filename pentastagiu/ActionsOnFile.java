package pentastagiu;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ActionsOnFile {
	public String FilePath;
	
	public ActionsOnFile() { }
	
	public ActionsOnFile(String filePath) {
		FilePath = filePath;
	}

	public String getFilePath() {
		return FilePath;
	}

	public void setFilePath(String filePath) {
		FilePath = filePath;
	}

	public void WriteInputToFile(String fileContent) {
		Path path = Paths.get(this.FilePath);
		try (BufferedWriter writer = Files.newBufferedWriter(path))
		{
		    writer.write(fileContent);
		} catch (IOException ex) {
			// I didn't catch FileNotFoundException because the file is created automatically if it doesn't exist
			System.out.println("Something went wrong while writing to file.\n " + ex.getMessage());
		}
	}
}
