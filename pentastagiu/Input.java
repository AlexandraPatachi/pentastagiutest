package pentastagiu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Input {
	public Input() { }
	
	public String ReadInputFromConsole() {
        String content = "";
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));) 
		{
			String line = reader.readLine();
			while(line != null && !line.isEmpty()) {
				content += (line + "\n");
				line = reader.readLine();
			}
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading from console.\n " + ex.getMessage());
		} 
		return content;
	}
}
