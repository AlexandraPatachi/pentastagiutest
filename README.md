##Patachi Alexandra - Test Java PentaStagiu

---

**Write a Java program that receives input from the console and then writes it into a file.**

Better to have the following elements:

1. OOP approach, meaning you have at least one more classes besides the main class.
2. Java 8 functionality for writing file contents.
3. Correct try-catch exception management implemented.

---

**Short description of used classes:**

1. Program = the main class.
2. Input = class for reading input from console.
3. ActionsOnFile = class for writing to file.
